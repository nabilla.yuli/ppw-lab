from django.db import models
from django.utils import timezone

# Create your models here.
class Activity(models.Model):
    ac_name = models.CharField(max_length=27)
    ac_time = models.DateTimeField(auto_now_add = True)
    ac_place = models.TextField()
    ac_category = models.CharField(max_length = 27)
