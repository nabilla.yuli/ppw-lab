from django import forms
    
class AcForm(forms.Form):
    attrs = {'class': 'form-control'}

    ac_name = forms.CharField(label='Nama Kegiatan', required=True, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=attrs))
    ac_time = forms.DateTimeField(label='Tanggal', required = True, widget=forms.DateTimeInput(attrs={'type':'datetime-local', 'class': 'form-control'}))
    ac_place = forms.CharField(label='Tempat', widget=forms.TextInput(attrs=attrs), required=True)
    ac_category = forms.CharField(label='Kategori', required=True, max_length=27, empty_value='Anonymous', widget = forms.TextInput(attrs=attrs))