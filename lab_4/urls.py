from django.conf.urls import url
from .views import index, tamu, activity, post_form, activity_table

urlpatterns = [
    url('index', index),
    url('bukutamu', tamu),
    url('post_form', post_form, name = 'post_form'),
    url('table', activity_table, name = 'table'),
    url('formKegiatan', activity),
    url('', index)
]
