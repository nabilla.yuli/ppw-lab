from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import AcForm
from .models import Activity

# Create your views here.
response = {'author' : "Nabilla Yuli Shafira"}

def index(request):
    return render(request, 'myprofile.html')

def tamu(request):
    return render(request, 'BukuTamu.html')

def post_form(request):
    if (request.method == 'POST'):
        response['name'] = request.POST['ac_name']
        response['time'] = request.POST['ac_time']   
        response['place'] = request.POST['ac_place']
        response['category'] = request.POST['ac_category'] 
        print(response['name'])
        result = Activity(ac_name=response['name'], ac_time=response['time'], ac_place=response['place'], ac_category=response['category'])     
        result.save()

        html = 'formKegiatan.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab_4/')

def activity(request):
    html = 'formKegiatan.html'
    response['post_form'] = AcForm
    return render(request, html, response)

def activity_table(request):
    if request.method == 'POST':
        Activity.objects.all().delete()
        return HttpResponseRedirect('/table/')
        
    activities = Activity.objects.all()
    response['activities'] = activities
    html = 'table.html'
    return render(request, html, response)

